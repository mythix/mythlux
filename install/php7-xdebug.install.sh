#!/bin/bash

#sudo apt-get update

cd /tmp
rm -rf xdebug
git clone https://github.com/derickr/xdebug.git --depth=1
cd xdebug
git fetch --tags
git checkout tags/XDEBUG_2_3_3
git clean -d -x -f

/usr/local/php7/bin/phpize
./configure --enable-xdebug --with-php-config=/usr/local/php7/bin/php-config
make
# make test
sudo make install
