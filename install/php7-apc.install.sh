#!/bin/bash

#sudo apt-get update

cd /tmp
rm -rf apc
git clone https://github.com/krakjoe/apcu.git apc
cd apc
#git fetch --tags
git checkout master
git clean -d -x -f

/usr/local/php7/bin/phpize
./configure --with-php-config=/usr/local/php7/bin/php-config
make
# make test
sudo make install


cd /tmp
rm -rf apc_bc
git clone https://github.com/krakjoe/apcu-bc.git apc_bc
cd apc_bc
git clean -d -x -f

/usr/local/php7/bin/phpize
./configure --with-php-config=/usr/local/php7/bin/php-config
make
# make test
sudo make install
