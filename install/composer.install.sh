#!/usr/bin/env bash

curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer.phar

echo '#!/usr/bin/env bash

php -c /etc/php5/php-nox.ini /usr/local/bin/composer.phar "$@"' | sudo tee /usr/local/bin/composer
sudo chmod +x /usr/local/bin/composer
