#!/usr/bin/env bash

set -e
set -o pipefail

# install and enabled apache and required modules

sudo apt-get install apache2 libapache2-mod-cgi php5 php5-cgi php5-cli
sudo a2enmod cgi actions alias rewrite
sudo a2dismod php5

# add a default ServerName

echo "
ServerName localhost" | sudo tee -a /etc/apache2/apache2.conf

# copy the default php5 cgi config and adapt for php7

sed 's,/cgi-bin/php5$,/cgi-bin/php7,g;s,/cgi-bin/php$,/cgi-bin/php7,g;s,\[345\],\[3457\],g' /etc/apache2/conf-available/php5-cgi.conf | sudo tee /etc/apache2/conf-available/php7-cgi.conf

# enabled the correct configs

sudo a2enconf serve-cgi-bin
# disable php cgi by default, needs to be eneabled in specific vhosts
sudo a2disconf php5-cgi php7-cgi
