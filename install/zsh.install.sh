#!/usr/bin/env bash

sudo apt install zsh fonts-powerline
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

PLUGIN_SUGGESTIONS=$(cd "${ZSH_CUSTOM:-${HOME}/.oh-my-zsh/custom}/plugins/zsh-autosuggestions" && pwd)
if [ -d "$PLUGIN_SUGGESTIONS" ]; then
	(cd "$PLUGIN_SUGGESTIONS" && git pull)
else
	git clone https://github.com/zsh-users/zsh-autosuggestions "${PLUGIN_SUGGESTIONS}"
fi

chsh -s $(which zsh)

if [ -f "${MYTHLUX_CONF_DIR}/templates/zshrc" ]; then
	cp "${MYTHLUX_CONF_DIR}/templates/zshrc" ~/.zshrc
fi
