#!/usr/bin/env bash

dir=`dirname "$(readlink -f "$0")"` || `echo 'failed to determine file location' && exit 1`
bindir=$(readlink -f ${dir}/../bin)

sudo ln -sf ${bindir}/* /usr/local/bin/
