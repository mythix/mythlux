#!/bin/bash

set -e
set -o pipefail

sudo apt-get update
sudo apt-get install -y php5-cgi git-core autoconf bison libxml2-dev libbz2-dev libmcrypt-dev libcurl4-openssl-dev libltdl-dev libpng-dev libpspell-dev libreadline-dev libicu-dev libssl-dev
sudo mkdir -p /etc/php7/conf.d
sudo mkdir -p /etc/php7/cli/conf.d
sudo mkdir -p /usr/local/php7

cd /tmp
rm -rf php-src
git clone https://github.com/php/php-src.git --depth=1
cd php-src
git fetch --tags
git checkout tags/php-7.0.0
git clean -d -x -f

./buildconf --force
./configure --prefix=/usr/local/php7 --enable-bcmath --with-bz2 --enable-calendar --enable-exif --enable-dba --enable-ftp --with-gettext --with-gd --enable-mbstring --with-mcrypt --with-mhash --enable-mysqlnd --with-mysqli=mysqlnd --with-pdo-mysql=mysqlnd --with-openssl --enable-pcntl --with-pspell --enable-shmop --enable-soap --enable-sockets --enable-sysvmsg --enable-sysvsem --enable-sysvshm --enable-wddx --with-zlib --enable-zip --enable-intl --with-readline --with-curl --with-config-file-path=/etc/php7/cli --with-config-file-scan-dir=/etc/php7/cli/conf.d
make
# make test
sudo make install

sudo ln -sf /usr/local/php7/bin/php /usr/local/bin/php7
sudo ln -sf /usr/local/php7/bin/php-cgi /usr/lib/cgi-bin/php7
