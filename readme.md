# bash scripts

* manage 
  * bash aliases
  * hosts file
  * vhosts
* install projects from git
* download remote mysql database
* import mysql dumps

## installation

This project requires a configuration directory with the following structure:

```
/your/config/dir
    - vhosts/
    - hosts/
    - bash_alias/
```

When using the `project-configure` or `project-install` commands, the generated config files will also be written to this directory.

To tell mythlux where your config directory is, add the following global to your bash:

```
export MYTHLUX_CONF_DIR="/your/config/dir"
```

To install all executables globally (symlinks to `/usr/local/bin`):

```
install/bin.install.sh
```

> scripts will invoke sudo when needed, do not run the scripts themselves with sudo unless you know what you are doing

## managing bash aliases, vhosts and hosts

when using any of the `hostsfile-update` or `bash-update-aliases` commands, the existing files will be overridden!

### vhosts

When generating vhosts with `apache-update-vhosts`, only symlinks will be overridden. 
This command only symlinks hosts to `/etc/apache2/sites-available`, never to `/etc/apache2/sites-enabled`.
 
When using the `project-configure` or `project-install` command, an additional call will be made to `a2ensite` though.

## project install

This command installs a web project. It will perform the following steps:

* checkout git repo
* check if composer is present, execute `composer install`
* check if symfony app/cache and app/logs is present, gives permissions (requires `setfacl`)
* creates vhost
* create hosts file config
* updates vhosts and hosts file (!overrides existing file!)
