#!/usr/bin/env bash

if [ -z "$MYTHLUX_CONF_DIR" ]; then
	echo "please install config and add the following line to ~/.bashrc:"
	echo "export MYTHLUX_CONF_DIR=\"/workspace/var/config/mythlux_config\""

	exit 1
fi

UNFORMAT="\e[39m"
GREEN="\e[32m"
YELLOW="\e[33m"

echo -e "${GREEN}installing apt packages${UNFORMAT}"

sudo apt install -y \
	vim \
	git \
	gnome-tweaks \
	gnome-shell-extensions \
	google-chrome-stable \
	nfs-kernel-server \
	virtualbox \
	vagrant \
	shutter \
	vpnc \
	network-manager-vpnc network-manager-vpnc-gnome \
	fonts-font-awesome \
	hddtemp \

echo -e "${GREEN}configuring git${UNFORMAT}"

GIT_USER=$(git config --global user.name)
if [ -z "$GIT_USER" ]; then
	echo -e "${YELLOW}git user.name:${UNFORMAT}"
	read GIT_USERNAME
	git config --global user.name "$GIT_USER"
else
	echo -e "${YELLOW}git user already configured to: ${GIT_USER}${UNFORMAT}"
fi
GIT_EMAIL=$(git config --global user.email)
if [ -z "$GIT_EMAIL" ]; then
        echo -e "${YELLOW}git user.email:${UNFORMAT}"
        read GIT_EMAIL
	git config --global user.email "$GIT_EMAIL"
else
	echo -e "${YELLOW}git email already configured to: ${GIT_EMAIL}${UNFORMAT}"
fi

echo -e "${GREEN}installing software from PPAs adapta gnome theme and paprirus icons${UNFORMAT}"
echo "  - adapta gnome theme"
echo "  - paprirus icons"
echo "  - conky"
sudo add-apt-repository -y ppa:papirus/papirus && \
	sudo apt-add-repository -y ppa:tista/adapta && \
	sudo apt update && \
	sudo apt install -y adapta-gtk-theme papirus-icon-theme libreoffice-style-papirus filezilla-theme-papirus conky-all conky-lua conky

echo -e "${GREEN}installing custom bin${UNFORMAT}"
. ./install/bin.install.sh
echo -e "${GREEN}installing ZSH${UNFORMAT}"
. ./install/zsh.install.sh
echo -e "${GREEN}conky config${UNFORMAT}"
if [ -f "${MYTHLUX_CONF_DIR}/templates/zshrc" ]; then
        cp "${MYTHLUX_CONF_DIR}/templates/conkyrc" ~/.conkyrc
fi
